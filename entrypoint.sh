#!/bin/bash

CMD="stern"
DEFAULT_QUERY='".*"'

if [[ -z "${QUERY}" ]]; then
  QUERY=$DEFAULT_QUERY
fi

if [[ -z "${MAX_FILE_SIZE}" ]]; then
  MAX_FILE_SIZE=10M
fi

CMD="$CMD $QUERY"

#if [[ -z "${JQ_QUERY}" ]]; then
#  :
#else
#  CMD="$CMD -o json | jq '.podName + \" \" + (.message | fromjson | ${JQ_QUERY})'"
#fi

CMD="$CMD | rotatelogs -e -t /var/log/stern.log $MAX_FILE_SIZE"
echo $CMD
eval $CMD