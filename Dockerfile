FROM ubuntu:focal

ARG VERSION

LABEL \
  org.opencontainers.image.title="stern" \
  org.opencontainers.image.authors="Vincenzo Santucci - Sandro Spadaro" \
  org.opencontainers.image.vendor="" \
  org.opencontainers.image.url="local" \
  org.opencontainers.image.source="https://gitlab.com/howtodoit/kubernetes/tools/stern.git" \
  org.opencontainers.image.version="$VERSION" \
  vendor="" \
  name="stern" \
  version="$VERSION" \
  summary="Stern K8s Multi Pod Log" \
  description="Stern K8s Multi Pod Log"

ENV QUERY=

COPY ./entrypoint.sh /root/entrypoint.sh

RUN apt-get update -y && \
    apt-get install jq curl -y && \
    apt-get install apache2-utils -y && \
    mkdir /root/.kube && \
    touch /var/log/stern.log && \
    chmod +x /root/entrypoint.sh

RUN curl -LO https://github.com/stern/stern/releases/download/v1.21.0/stern_1.21.0_linux_amd64.tar.gz && \
    tar -xzvf stern_1.21.0_linux_amd64.tar.gz && \
    install -o root -g root -m 0755 stern /usr/local/bin/stern && \
    stern -v

WORKDIR /root

ENTRYPOINT /root/entrypoint.sh